using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementController))]

public class PlayerController : MonoBehaviour
{
    [Header("Movimiento de Cam")]
    public Vector2 mouseMovement;
    public Camera playerCamera;
    public float rotateCameraX;
    public float rotatePlayerY;
    public float sencibilidadRaton;

    [Header("Movemen Controller")]
    public MovementController movement;
    public MovementController rotate;


    private void Update()
    {
        //Capturar el movimiento del pj

        mouseMovement.x = Input.GetAxis("Mouse X") * sencibilidadRaton;
        mouseMovement.y = Input.GetAxis("Mouse Y") * sencibilidadRaton;



        //Rotacion 
        rotateCameraX -= mouseMovement.y;
       

        rotateCameraX = Mathf.Clamp(rotateCameraX, -40, 40);


        playerCamera.transform.localRotation = Quaternion.Euler(rotateCameraX, 0, 0);
        movement.Move(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        rotate.Rotate(Input.GetAxis("Mouse X"));
    }
}
