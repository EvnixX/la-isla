using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolaB : MonoBehaviour
{
    public GameObject balaPrefab;
    public Transform throwPoint;
    public float force;
    public int cargador = 14;
    public int recargar;
    private void Start()
    {
        cargador = recargar;
    }


    private void Update()
    {
      if(Input.GetButtonDown("Fire1") && cargador >= 0)
        {
            GameObject go = Instantiate(balaPrefab, throwPoint.transform.position, throwPoint.transform.rotation);
            go.GetComponent<Rigidbody>().AddForce(transform.right * force, ForceMode.Impulse);
            Destroy(go, 2f);
            cargador--;
        }

      if(Input.GetButtonDown("Recargar"))
        {
            cargador = recargar;
        }
      
       
    }

    
}
