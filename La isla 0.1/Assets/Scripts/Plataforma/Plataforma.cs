using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    public GameObject plataform;

    public Transform minPosition;
    public Transform maxPosition;

    public float speedMoviment;



    private void OnTriggerStay(Collider other)
    {
        if (other != null)
        {
            MovePlataform();
        }
        
    }

    private void MovePlataform()
    {
        plataform.transform.Translate(Vector3.up * Time.deltaTime * speedMoviment);

        
        if (plataform.transform.position.y >= maxPosition.position.y && speedMoviment > 0)
        {
            speedMoviment = speedMoviment * -1;
        }

        if (plataform.transform.position.y <= minPosition.position.y && speedMoviment < 0)
        {
            speedMoviment = speedMoviment * -1;
        }

    }
    
    

}
