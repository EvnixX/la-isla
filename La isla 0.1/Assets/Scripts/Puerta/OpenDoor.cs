using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{

    public Transform cameraPlayer;
    public Transform objetoVacio;
    public Transform boxArma;
    public GameObject arma;
    public LayerMask im;
    public float rayDistance;

    public void Update()
    {
        if (Input.GetButtonDown("Interactuar"))
        {
            if (objetoVacio.childCount > 0)
            {
                objetoVacio.GetComponentInChildren<Rigidbody>().isKinematic = false;
                objetoVacio.DetachChildren();

                if (boxArma.childCount > 0)
                {
                    boxArma.GetChild(0).gameObject.SetActive(true);

                }
            }

            else
            {
                Debug.DrawRay(cameraPlayer.position, cameraPlayer.forward * rayDistance, Color.black);
                if (Physics.Raycast(cameraPlayer.position, cameraPlayer.transform.forward, out RaycastHit hit, rayDistance, im))
                {
                    hit.transform.GetComponent<Rigidbody>().isKinematic = true;
                    hit.transform.parent = objetoVacio;
                    hit.transform.localPosition = Vector3.zero;
                    hit.transform.localRotation = Quaternion.Euler(0, 0, 0);

                    if (boxArma.childCount > 0)
                    {
                        boxArma.GetChild(0).gameObject.SetActive(false);

                    }
                }
            }

        }
    }

    public PlayerStatic playerStac;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Door" && playerStac.Hamburguesa >= 3)
        {
            other.GetComponentInParent<Door>().OnOpenDoor();
        }

        if (other.tag == "Hamburguesa")
        {
            playerStac.Hamburguesa++;
        }

        if (other.tag == "Pistola")
        {
            arma.transform.parent = boxArma;
            arma.transform.transform.localPosition = Vector3.zero;
            arma.transform.localRotation = Quaternion.Euler(0, 0, 0);

            if (objetoVacio.childCount > 0)
            {
                other.gameObject.SetActive(false);
            }
        }
    }

}
