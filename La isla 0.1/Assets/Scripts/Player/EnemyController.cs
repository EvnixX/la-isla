using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementController))]

public class EnemyController : MonoBehaviour
{
    
    public MovementController movement;
    public Transform jugador;
    public float rangeOfDetection = 15f;
   
    

    public void Update()
    {
        float distance = Vector3.Distance(transform.position, jugador.transform.position);

        if(distance <= rangeOfDetection)
        {
            Vector3 forward = transform.transform.TransformDirection(Vector3.forward.normalized);
            Vector3 toOther = jugador.position - transform.transform.position.normalized;



            if (Vector3.Dot(forward, toOther) > -1)
            {
              
                movement.Rotate(1);
            }

            if (Vector3.Dot(forward, toOther) < 1)
            {
              
                movement.Rotate(1);
            }
            Debug.Log("Amenaza detectada");
           

        }


        

    }
    
}
