using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour

{
    // Movimiento de personaje

    [Header("Movimiento de Pj")]
    public float SpeedMovement;
    public Vector3 Direccion;
    public CharacterController Controller;


    // Saltitos personaje

    [Header("Salto de Pj")]
    public Vector3 desplazamientoY;
    public float jumpHeigth;
    public float gravity = -9.8f;

    // rotacion
    [Header("Rotate")]
    public float rotatePlayerY;


    private void Update()

    {
        desplazamientoY.y += gravity * Time.deltaTime;
        Controller.Move(desplazamientoY * Time.deltaTime);

        if (Controller.isGrounded && desplazamientoY.y < 0)
        {
            desplazamientoY.y = -2f;

        }

        // Saltitos personaje
        if (Controller.isGrounded && Input.GetButtonDown("Jump"))
        {
            desplazamientoY.y = Mathf.Sqrt(jumpHeigth * -2f * gravity);
        }

            
    }

   public void Move(float horizontal, float vertical)
    {
        Direccion.x = horizontal;
        Direccion.z = vertical;

        Direccion = transform.TransformDirection(Direccion);

        Controller.Move(Direccion * Time.deltaTime * SpeedMovement);
        Controller.Move(desplazamientoY * Time.deltaTime);
    }

    public void Rotate(float rotateValue)
    {
        rotatePlayerY += rotateValue;
        Controller.transform.rotation = Quaternion.Euler(0, rotatePlayerY, 0);
    }

    
}




