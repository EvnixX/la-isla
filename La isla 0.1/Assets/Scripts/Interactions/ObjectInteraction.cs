using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInteraction : MonoBehaviour
{
  
    // Variable para el sonido 

    public AudioSource source;
    public AudioClip AudioFX;

   
    private void OnTriggerEnter(Collider other)
    {

        gameObject.SetActive(false);

        //Mover sonido 

        source.transform.position = transform.position;

        // reproducir el sonido

        source.PlayOneShot(AudioFX);


        // Recolector de bateria

        
    }

}
